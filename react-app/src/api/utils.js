export const BACKEND_URL = "https://api.pilloww.benjaminreich.de";

export const resourceTypes = {
  normalCare: "normalCare",
  intermediateCare: "intermediateCare",
  intensiveCare: "intensiveCare"
};

export const createQueryString = query =>
  Object.keys(query)
    .filter(key => query[key])
    .map(key => `${key}=${query[key] || ""}`)
    .join("&");

export const addQueryString = (url, query) => {
  const queryStringParams = createQueryString(query || {});
  return query
    ? `${url}${queryStringParams ? `?${queryStringParams}` : queryStringParams}`
    : url;
};



export const resourceUtilizationToStateName = ({ available_amount, utilized_amount }) => {
  // FIXME: Dieser State sollte idealerweise aus dem Backend kommen.
  if (available_amount === 0) return "white";
  const ratio = utilized_amount / available_amount;
  if (ratio > 0.9) return "danger";
  if (ratio > 0.75) return "warning";
  return "success";
};

export const worstResourceUtilizationStateName = resources => {
  let worstStateName = 'success';
  resources.forEach(resource => {
    const stateName = resourceUtilizationToStateName(resource);
    if (stateName === 'danger') worstStateName = stateName;
    if (stateName === 'warning' && worstStateName === 'success') worstStateName = stateName;
  });
  return worstStateName;
};
