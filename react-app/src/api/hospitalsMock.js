import faker from "faker";
import { resourceTypes } from "./utils";

const NUM_FAKE_HOSPITALS = 158;

const fakeResource = (max, type) => {
  const available_amount = faker.random.number(max);
  return {
    id: faker.random.number(),
    type,
    label: type,
    available_amount,
    utilized_amount: faker.random.number(available_amount)
  };
};

export const getAllHospitals = () => {
  const list = [];

  for (let idx = 0; idx < NUM_FAKE_HOSPITALS; idx++) {
    list.push({
      id: idx === 0 ? '834583fc-cb24-4f70-b677-69913caade3e' : idx,
      name: faker.company.companyName(),
      address: {
        locality: faker.address.city(),
        city: faker.address.city(),
        address_line1: `${faker.address.streetName()} ${faker.address.streetAddress()}`,
        address_line2: faker.address.secondaryAddress(),
        postcode: faker.address.zipCode()
      },
      geolocation: {
        latitude: faker.address.latitude(),
        longitude: faker.address.longitude()
      },
      resources: [
        fakeResource(100, resourceTypes.intensiveCare),
        fakeResource(200, resourceTypes.intermediateCare),
        fakeResource(500, resourceTypes.normalCare)
      ]
    });
  }

  return list;
};
