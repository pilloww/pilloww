import axios from "axios";
import { BACKEND_URL, addQueryString } from "./utils";

// Swagger: https://api.pilloww.benjaminreich.de/docs

const headers = {
  "Access-Control-Allow-Origin": "*"
};

export const getAllHospitals = async () => {
  const rsp = await axios.get(`${BACKEND_URL}/hospitals?limit=100`, {
    headers
  });

  return rsp.data.hospitals.slice(0, 50);
};

export const getResource = queryParams => {
  const completeUrl = addQueryString(`${BACKEND_URL}/resource`, queryParams, {
    headers
  });
  return axios
    .get(completeUrl, { params: queryParams })
    .then(response => response.data);
};

export const saveResource = resource => {
  resource.resources.forEach(singleResource => {
    axios.post(
      `${BACKEND_URL}/resource/availability/${singleResource.id}`,
      { count: singleResource.available_amount },
      {
        headers
      }
    );
    axios.post(
      `${BACKEND_URL}/resource/utilization/${singleResource.id}`,
      { count: singleResource.utilized_amount },
      {
        headers
      }
    );
  });
  return;
};

export const getUtilizationModel = async (hospitalId, data) => {
  const response = await axios.post(
    `${BACKEND_URL}/hospital/model_utilization/${hospitalId}`,
    data
  );
  return response.data;
};
