import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import Container from "react-bulma-components/lib/components/container";
import Login from "./components/templates/Login";
import HospitalsList from "./components/templates/HospitalsList";
import EnterVacantBeds from "./components/templates/EnterVacantBeds";
import MenuBar from "./components/molecules/MenuBar";
// import { getAllHospitals } from "./api/hospitalsMock";
import { getAllHospitals, saveResource } from "./api/hospitalsApi";
import translations from "./common/translations";
import HospitalsMap from "./components/templates/HospitalsMap";
import HospitalDetail from "components/templates/HospitalDetail";

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources: translations,
    lng: "de",
    fallbackLng: "de"
  });

function App() {
  const { t } = useTranslation();

  const [capacityList, setCapacityList] = useState([]);
  const [user, setUser] = useState(null);

  useEffect(() => {
    const getData = async () => {
      const hospitalData = await getAllHospitals();
      const userData = {
        id: "834583fc-cb24-4f70-b677-69913caade3e",
        lang: "de",
        name: "Max Mustermann"
      };
      setCapacityList(hospitalData);
      setUser(userData);
    };

    getData();
  }, []);

  const login = userData => {
    setUser(userData);
  };

  const logout = () => {
    setUser(null);
  };

  const setHospitalItem = hospitalItem => {
    const index = capacityList.findIndex(item => item.id === hospitalItem.id);
    const capacityListToUpdate = capacityList.slice();
    capacityListToUpdate[index] = hospitalItem;
    setCapacityList(capacityListToUpdate);
    saveResource(hospitalItem);
  };

  return (
    <Container>
      <Router>
        <MenuBar user={user} logout={logout} t={t} />
        {capacityList.length === 0 && (
          <progress
            className="progress main-progress is-small is-primary"
            max="100"
          >
            15%
          </progress>
        )}
        <Switch>
          <Route exact path="/">
            <HospitalsList user={user} capacityList={capacityList} t={t} />
          </Route>
          <Route exact path="/login">
            <Login user={user} login={login} t={t} />
          </Route>
          <Route exact path="/map">
            <HospitalsMap hospitals={capacityList} t={t} />
          </Route>
          <Route exact path="/hospital/:id">
            <HospitalDetail capacityList={capacityList} t={t} />
          </Route>
          {user && (
            <Route exact path="/enter-vacant-beds">
              <EnterVacantBeds
                user={user}
                capacityList={capacityList}
                setHospitalItem={setHospitalItem}
                t={t}
              />
            </Route>
          )}
        </Switch>
      </Router>
    </Container>
  );
}

export default App;
