const translations = {};

translations.de = {
  translation: {
    hospitalsList: {
      title: "Krankenhäuser",
      searchField: "Name, Ort oder PLZ"
    },
    diagram: {
      expectedPatients: "Zu erwartende Auslastung"
    },
    common: {
      save: "Übernehmen",
      cancel: "Abbrechen",
      next: "Nächste",
      previous: "Vorherige",
      results: "Ergebnisse",
      result: "Ergebnis",
      noResults: "Keine Ergebnisse.",
      redirectToHome: "Sie werden wieder auf die Startseite zurückgeleitet."
    },
    resources: {
      normalCare: "Standard-Betten",
      intermediateCare: "Intermediate-Betten",
      intensiveCare: "Intensiv-Betten"
    },
    nav: {
      home: "Home",
      map: "Karte",
      submitCapacity: "Bettenkapazität erfassen",
      signUp: "Registrieren",
      login: "Login",
      logout: "Logout"
    },
    login: {
      headline: "Login",
      email: "E-Mail",
      password: "Passwort",
      login: "Einloggen",
      errorMessage: "Fehler beim Einloggen: E-Mail oder Passwort nicht korrekt"
    },
    submitCapacity: {
      heading: "Bettenkapazität erfassen",
      free: "Frei",
      utilized: "Belegt",
      total: "Gesamt",
      success: {
        headline: "Daten wurden übernommen",
        body: "Vielen Dank für das Update!"
      }
    },
    hospitalCard: {
      lastUpdated: "Zuletzt aktualisiert"
    },
    hospital: {
      heading: "Detailansicht und Vorhersage"
    }
  }
};

export default translations;
