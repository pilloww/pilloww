import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

const AmountsInput = ({
  handlePlusMinusButton,
  handleBedsInputs,
  resource,
  amountType
}) => {
  const generateChangeButtons = numbers =>
    numbers.map((amount, index) => (
      <button
        key={index}
        className="button is-medium is-info"
        onClick={() => handlePlusMinusButton(amountType, amount)}
      >
        {`${amount > 0 ? "+" : ""}${amount}`}
      </button>
    ));

  return (
    <div className="control beds-input-control">
      {generateChangeButtons([-10, -1])}
      <input
        className="input is-large"
        name="utilized_amount"
        onChange={handleBedsInputs}
        value={resource[amountType]}
      />
      {generateChangeButtons([+1, +10])}
    </div>
  );
};

export default function BedsInput(props) {
  const { title, updateResource } = props;
  const [resource, setResource] = useState(props.resource);

  const { t } = useTranslation();

  const handleBedsInputs = event => {
    const name = event.target.name;
    let value = parseInt(event.target.value);
    if (event.target.value === "") {
      value = "";
    }
    if (isNaN(value) && value !== "") {
      value = 0;
    }

    const updatedResource = { ...resource, [name]: value };
    setResource(updatedResource);
    updateResource(updatedResource);
  };

  // useEffect(() => {
  //   let updatedResource = {...resource};
  //   if(resource.available_amount < resource.utilized_amount) {
  //     updatedResource = {...resource, utilized_amount: resource.available_amount};
  //     setResource(updatedResource);
  //   }
  //   updateResource(updatedResource);
  // }, [resource.utilized_amount]);

  // useEffect(() => {
  //   let updatedResource = {...resource};
  //   if(resource.available_amount < resource.utilized_amount) {
  //     updatedResource = {...resource, utilized_amount: resource.available_amount};
  //     setResource(updatedResource);
  //   }
  //   updateResource(updatedResource);
  // }, [resource.available_amount]);

  const handlePlusMinusButton = (name, amount) => {
    if (
      (name === "utilized_amount" &&
        resource.available_amount < resource.utilized_amount + amount) ||
      (name === "available_amount" &&
        resource.available_amount + amount < resource.utilized_amount) ||
      resource[name] + amount < 0
    ) {
      return;
    }

    const updatedResource = { ...resource, [name]: resource[name] + amount };
    setResource(updatedResource);
    updateResource(updatedResource);
  };

  return (
    <>
      <h3 className="title is-3">{title}</h3>
      <div className="field" style={{ marginBottom: "2rem" }}>
        <label className="label has-text-centered">
          {t("submitCapacity.utilized")}
        </label>
        <AmountsInput
          handlePlusMinusButton={handlePlusMinusButton}
          handleBedsInputs={handleBedsInputs}
          resource={resource}
          amountType="utilized_amount"
        />
      </div>
      <div className="field">
        <label className="label has-text-centered">
          {t("submitCapacity.total")}
        </label>
        <AmountsInput
          handlePlusMinusButton={handlePlusMinusButton}
          handleBedsInputs={handleBedsInputs}
          resource={resource}
          amountType="available_amount"
        />
      </div>
    </>
  );
}
