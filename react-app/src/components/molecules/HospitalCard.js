import React from "react";
import { Card, Content, Columns, Heading } from "react-bulma-components";
import { Link } from "react-router-dom";
import AddressSection from "../atoms/AddressSection";
import CapacityTag from "../atoms/CapacityTag";

const HospitalCard = ({ hospital: { name, address, resources, id }, t }) => {
  const resourceDates = resources.flatMap(
    ({ available_amount_timestamp, utilized_amount_timestamp }) => [
      new Date(available_amount_timestamp * 1000),
      new Date(utilized_amount_timestamp * 1000)
    ]
  );
  const latestDate =
    !!resourceDates.length && new Date(Math.max.apply(null, resourceDates));

  return (
    <Card className="card-hospital">
      <Card.Content>
        <Columns>
          <Columns.Column>
            <Heading size={5}><Link to={`/hospital/${id}`}>{name}</Link></Heading>
            {address && <AddressSection address={address} collapsible />}
          </Columns.Column>
          <Columns.Column>
            <Content>
              <Columns>
                {resources.map(resource => (
                  <Columns.Column key={resource.id}>
                    <CapacityTag resource={resource} t={t} />
                  </Columns.Column>
                ))}
              </Columns>
              <p>
                {t("hospitalCard.lastUpdated")}: {latestDate.toLocaleString()}
              </p>
            </Content>
          </Columns.Column>
        </Columns>
      </Card.Content>
    </Card>
  );
};

export default HospitalCard;
