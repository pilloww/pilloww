import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Navbar } from "react-bulma-components";

const MenuBar = ({ user, logout, t }) => {
  const [isMenuOpen, setMenuOpen] = useState(false);

  return (
    <Navbar>
      <Navbar.Brand>
        <Link to={"/"}>
          <img src={process.env.PUBLIC_URL + "/logo-small.png"} alt="Pillow Logo" className="navbar-logo"/>
        </Link>
        <a
          className={"navbar-burger burger" + (isMenuOpen ? " is-active" : "")}
          onClick={() => setMenuOpen(!isMenuOpen)}
          role="button"
          aria-label="menu"
          aria-expanded="false"
        >
          <span aria-hidden="true" />
          <span aria-hidden="true" />
          <span aria-hidden="true" />
        </a>
      </Navbar.Brand>
      <Navbar.Menu className={isMenuOpen ? " is-active" : ""}>
        <Navbar.Container>
          <Link
            to={"/"}
            className="navbar-item"
            onClick={() => isMenuOpen && setMenuOpen(false)}
          >
            {t("nav.home")}
          </Link>
          <Link
            to={"/map"}
            className="navbar-item"
            onClick={() => isMenuOpen && setMenuOpen(false)}
          >
            {t("nav.map")}
          </Link>
          {user && (
            <Link
              to={"/enter-vacant-beds"}
              className="navbar-item"
              onClick={() => isMenuOpen && setMenuOpen(false)}
            >
              {t("nav.submitCapacity")}
            </Link>
          )}
        </Navbar.Container>
        <Navbar.Container position="end">
          <div className="navbar-item">{user?.name}</div>
          <div className="buttons">
            {user ? (
              <Navbar.Item className="button is-light" onClick={logout}>
                {t("nav.logout")}
              </Navbar.Item>
            ) : (
              <Link to={"/login"} className="button is-primary">
                {t("nav.login")}
              </Link>
            )}
          </div>
        </Navbar.Container>
      </Navbar.Menu>
    </Navbar>
  );
};

export default MenuBar;
