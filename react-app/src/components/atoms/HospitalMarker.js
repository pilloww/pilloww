import React from "react";
import { Link } from "react-router-dom";
import { Marker, Popup } from "react-leaflet";
import { Icon } from 'leaflet';
import { worstResourceUtilizationStateName } from "../../api/utils.js";
import CapacityTag from "./CapacityTag.js";

// FIXME: markerIcons eleganter erzeugen ;)
const markerIcons = {
  success: new Icon({
    iconUrl: '/marker-icon-success.png',
    iconSize: [50/2, 82/2],
    iconAnchor: [24/2, 81/2],
    popupAnchor: [0, -76/2]
  }),
  warning: new Icon({
    iconUrl: '/marker-icon-warning.png',
    iconSize: [50/2, 82/2],
    iconAnchor: [24/2, 81/2],
    popupAnchor: [0, -76/2]
  }),
  danger: new Icon({
    iconUrl: '/marker-icon-danger.png',
    iconSize: [50/2, 82/2],
    iconAnchor: [24/2, 81/2],
    popupAnchor: [0, -76/2]
  })
}

const HospitalMarker = ({ hospital, t }) => {
  const {
    name,
    address,
    geolocation: { latitude, longitude },
    resources,
    id
  } = hospital;
  return (
    <Marker position={[latitude, longitude]} icon={markerIcons[worstResourceUtilizationStateName(resources)]}>
      <Popup>
        <strong><Link to={`/hospital/${id}`}>{name}</Link></strong>
        <br />
        {address && address.locality}
        <br />
        {resources.map(resource => <CapacityTag resource={resource} t={t} key={resource.id} />)}
      </Popup>
    </Marker>
  );
};

export default HospitalMarker;
