import React from "react";
import { Tag } from "react-bulma-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faProcedures,
  faBed,
  faStethoscope
} from "@fortawesome/free-solid-svg-icons";
import {
  resourceTypes,
  resourceUtilizationToStateName
} from "../../api/utils.js";

const stateClassNameForResource = resource =>
  "is-" + resourceUtilizationToStateName(resource);

const getIcon = label => {
  if (label === resourceTypes.normalCare) return faBed;
  if (label === resourceTypes.intensiveCare) return faProcedures;
  if (label === resourceTypes.intermediateCare) return faStethoscope;
  return faBed;
};

const CapacityTag = ({ resource, t }) => {
  const { label, available_amount, utilized_amount, type } = resource;
  return (
    <div className="control">
      <div
        className="tags has-addons is-medium"
        title={t(`resources.${type}`) || label}
      >
        <Tag color="dark">
          <FontAwesomeIcon icon={getIcon(type)} size="1x" />
        </Tag>
        <span className={`tag ${stateClassNameForResource(resource)}`}>
          {available_amount - utilized_amount} / {available_amount}
        </span>
      </div>
    </div>
  );
};

export default CapacityTag;
