import React from "react";
import PropTypes from "prop-types";
import Collapsible from "react-collapsible";
import { faPlusCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const AddressDetailSection = ({ address_line1, address_line2, postcode }) => (
  <>
    <p>{address_line1}</p>
    {address_line2 && <p>{address_line2}</p>}
    <p>PLZ: {postcode}</p>
  </>
);

const AddressSection = ({
  collapsible,
  address: { locality, address_line1, address_line2, postcode }
}) => (
  <>
    <p>
      <strong>{locality}</strong>
    </p>
    {collapsible && (
      <Collapsible trigger={<FontAwesomeIcon icon={faPlusCircle} size="1x" />}>
        <AddressDetailSection
          address_line1={address_line1}
          address_line2={address_line2}
          postcode={postcode}
        />
      </Collapsible>
    )}
    {!collapsible && (
      <AddressDetailSection
        address_line1={address_line1}
        address_line2={address_line2}
        postcode={postcode}
      />
    )}
  </>
);

AddressSection.propTypes = {
  address: PropTypes.shape({
    locality: PropTypes.string,
    address_line1: PropTypes.string,
    address_line2: PropTypes.string,
    postcode: PropTypes.string
  }).isRequired,
  collapsible: PropTypes.bool
};

AddressSection.defaultProps = {
  collapsible: false
};

export default AddressSection;
