import React, { useState, useEffect } from "react";
import HospitalCard from "../molecules/HospitalCard";
import Pagination from "react-bulma-components/lib/components/pagination";
import {
  Field,
  Control,
  Input
} from "react-bulma-components/lib/components/form";
import Columns from "react-bulma-components/lib/components/columns";

const HospitalsList = ({ capacityList, t }) => {
  const pageSize = 25;
  const [filteredCapacityList, setFilteredCapacityList] = useState([]);
  const totalPages = Math.floor(filteredCapacityList.length / pageSize);
  const [page, setPage] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    setPage(1);
  }, [capacityList]);

  useEffect(() => {
    if (searchTerm !== "") {
      const tempList = capacityList.filter(item => {
        return checkSearchTerm(searchTerm, item);
      });
      setFilteredCapacityList(tempList);
    } else {
      setFilteredCapacityList(capacityList);
    }
  }, [searchTerm, capacityList]);

  function changePage(value) {
    setPage(value);
  }

  function onSearchTermEnter(event) {
    setSearchTerm(event.target.value);
  }

  function checkSearchTerm(searchTerm, item) {
    if (
      (item.name &&
        item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1) ||
      (item.address &&
        item.address.postcode &&
        item.address.postcode
          .toLowerCase()
          .indexOf(searchTerm.toLowerCase()) !== -1) ||
      (item.address &&
        item.address.locality &&
        item.address.locality
          .toLowerCase()
          .indexOf(searchTerm.toLowerCase()) !== -1)
    ) {
      return true;
    }
    return false;
  }

  return (
    <div className="container main-container">
      <h1 className="title is-1">{t("hospitalsList.title")}</h1>
      <Columns>
        <Columns.Column>
          {filteredCapacityList.length > 0 && filteredCapacityList.length}{" "}
          {filteredCapacityList.length === 1
            ? t("common.result")
            : filteredCapacityList.length === 0
            ? t("common.noResults")
            : t("common.results")}
        </Columns.Column>
        <Columns.Column>
          <Field>
            <Control>
              <Input
                placeholder={t("hospitalsList.searchField")}
                value={searchTerm}
                onChange={onSearchTermEnter}
              />
            </Control>
          </Field>
        </Columns.Column>
      </Columns>
      {filteredCapacityList
        .slice((page - 1) * pageSize, page * pageSize)
        .map(hospital => (
          <HospitalCard key={hospital.id} hospital={hospital} t={t} />
        ))}
      <Pagination
        className="is-centered"
        current={page}
        total={totalPages}
        delta={1}
        onChange={value => changePage(value)}
        next={t("common.next")}
        previous={t("common.previous")}
      />
    </div>
  );
};

export default HospitalsList;
