import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';

export default function Login({user, login, t}) {

  const history = useHistory();

  if(user) {
    history.push('/');
  }

  const [state, setState] = useState({
    email: '',
    password: '',
    isLoading: false,
    hasError: false
  });

  const handleInput = event => {
    const name = event.target.name;
    const value = event.target.value;
    setState({...state, [name]: value});
  };

  const handleLogin = async event => {
    event.preventDefault();

    setState({...state, hasError: false, isLoading: true});

    try {
      // TODO: await API-Call for login
      const userData = {
        id: 5,
        lang: 'de',
        name: 'Max Mustermann'
      };
      login(userData);
      history.push('/');
    } catch (e) {
      setState({...state, hasError: true});
    }

    setState({...state, isLoading: false});
  };

  return (
    <div className="container main-container">
      <h1 className="title is-1">{t("login.headline")}</h1>

      <form onSubmit={handleLogin}>
        <div className="field">
          <label className="label">{t("login.email")}</label>
          <div className="control">
            <input className="input" type="email" name="email" required
                   value={state.email} onChange={handleInput}/>
          </div>
        </div>

        <div className="field">
          <label className="label">{t("login.password")}</label>
          <div className="control">
            <input className="input" type="password" name="password" required
                   value={state.password} onChange={handleInput}/>
          </div>
        </div>

        {state.hasError ?
          <div className="notification is-danger">
            {t("login.errorMessage")}
          </div> : ''}

        <div className="field">
          <button type="submit" className="button is-success">
            {t("login.login")}
          </button>
        </div>
      </form>
    </div>
  );
}
