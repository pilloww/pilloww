import React from "react";
import { Map, TileLayer } from "react-leaflet";
import HospitalMarker from "components/atoms/HospitalMarker";

const defaultPositions = {
  BER: {
    latitude: 52.52,
    longitude: 13.4
  },
  FRA: {
    latitude: 50.11,
    longitude: 8.68
  },
  CGN: {
    latitude: 50.93,
    longitude: 6.96
  },
  center: {
    latitude: 51.0,
    longitude: 10.0
  }
};

const HospitalsMap = ({ hospitals, t }) => {
  const initialPosition = defaultPositions.center;
  const initialZoom = 6; // 12

  const coordinates = [initialPosition.latitude, initialPosition.longitude];
  return (
    <Map
      center={coordinates}
      zoom={initialZoom}
      style={{
        height: "550px"
      }}
    >
      <TileLayer
        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {hospitals.map(hospital => (
        <HospitalMarker key={hospital.id} hospital={hospital} t={t} />
      ))}
    </Map>
  );
};

export default HospitalsMap;
