import React, { useEffect, useState } from "react";
import { withRouter } from "react-router";
import {
  Container,
  Heading,
  Card,
  Media,
  Section,
  Columns
} from "react-bulma-components";
import { Line } from "react-chartjs-2";
import CapacityTag from "../atoms/CapacityTag.js";
import { getUtilizationModel } from "api/hospitalsApi";

const HospitalDetail = props => {
  const { t, capacityList } = props;

  const hospitalId = props.match.params.id;

  const [data, setData] = useState({});

  const diagramOptions = {
    scales: {
      xAxes: [
        {
          type: "time",
          time: {
            displayFormats: {
              millisecond: "DD.MM.YYYY",
              second: "DD.MM.YYYY",
              minute: "DD.MM.YYYY",
              hour: "DD.MM.YYYY",
              day: "DD.MM.YYYY",
              week: "DD.MM.YYYY",
              month: "DD.MM.YYYY",
              quarter: "DD.MM.YYYY",
              year: "DD.MM.YYYY"
            }
          }
        }
      ]
    }
  };

  useEffect(() => {
    const getData = async () => {
      const data = await getUtilizationModel(hospitalId, {
        growth_rate: 0.0000014,
        surrounding_population: 80000,
        expected_infected_fraction: 0.7,
        expected_hospitalization_fraction: 0.15,
        expected_hospitalization_period: 1209600
      });

      const dates = [];
      const points = [];

      data.samples.forEach(sample => {
        dates.push(new Date(sample[0] * 1000));
        points.push(sample[1]);
      });

      const formattedData = {
        labels: dates,
        datasets: [
          {
            label: t("diagram.expectedPatients"),
            fill: true,
            lineTension: 0.1,
            backgroundColor: "rgba(75,192,192,0.4)",
            borderColor: "rgba(75,192,192,1)",
            borderCapStyle: "butt",
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: "miter",
            pointBorderColor: "rgba(75,192,192,1)",
            pointBackgroundColor: "#fff",
            pointBorderWidth: 1,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: "rgba(75,192,192,1)",
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 2,
            pointRadius: 1,
            pointHitRadius: 10,
            data: points
          }
        ]
      };
      setData(formattedData);
    };
    getData();
  }, []);

  if (capacityList) {
    const hospital = capacityList.find(item => item.id === hospitalId);
    if (hospital) {
      return (
        <>
          <Container className="main-container">
            <Heading size={1}>{t("hospital.heading")}</Heading>

            <Card className="beds-input-hospital">
              <Card.Content>
                <Media>
                  <Media.Content>
                    <p className="title is-4">{hospital.name}</p>
                    {hospital.address && (
                      <>
                        {hospital.address.address_line1 && (
                          <p className="subtitle is-6">
                            {hospital.address.address_line1}
                          </p>
                        )}
                        {hospital.address.address_line2 && (
                          <p className="subtitle is-6">
                            {hospital.address.address_line2}
                          </p>
                        )}
                        <p className="subtitle is-6">
                          {hospital.address.postcode}{" "}
                          {hospital.address.locality}
                        </p>
                      </>
                    )}
                  </Media.Content>
                </Media>

                <Columns>
                  {hospital.resources.map(resource => (
                    <Columns.Column key={resource.id}>
                      <CapacityTag resource={resource} t={t} />
                    </Columns.Column>
                  ))}
                </Columns>
              </Card.Content>
            </Card>
            <Line data={data} options={diagramOptions}></Line>
          </Container>
        </>
      );
    }
  }

  return <p />;
};

export default withRouter(HospitalDetail);
