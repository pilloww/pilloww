import React, { useState } from "react";
import {
  Container,
  Heading,
  Card,
  Media,
  Section,
  Button
} from "react-bulma-components";
import BedsInput from "../molecules/BedsInput";
import { Redirect } from "react-router-dom";

const EnterVacantBeds = ({ user, t, setHospitalItem, capacityList }) => {
  const userHospital = capacityList.find(item => item.id === user.id);
  const [resources, setResources] = useState(userHospital.resources);
  const [redirect, setRedirect] = useState(false);
  const [successMessageVisible, setSuccessMessageVisible] = useState(false);

  const showSuccessMessage = () => {
    window.scrollTo(0, 0);
    setSuccessMessageVisible(true);
    setTimeout(() => {
      setSuccessMessageVisible(false);
      setRedirect(true);
    }, 5000);
  };

  const handleFormSubmit = () => {
    const updatedHospital = {
      ...userHospital,
      resources: resources
    };
    setHospitalItem(updatedHospital);
    showSuccessMessage();
  };

  const handleCancel = () => {
    // FIXME: Tut nix!
    setRedirect(true);
    // setResources(userHospital.resources.slice()); // FIXME: Wherefore?
  };

  const updateResource = resource => {
    const resourceIndex = resources.findIndex(r => r.id === resource.id);
    const newResources = resources.slice();
    newResources[resourceIndex] = resource;
    return setResources(newResources);
  };

  const { address, name } = userHospital;

  return !redirect ? (
    <Container className="main-container">
      <Heading size={1}>{t("submitCapacity.heading")}</Heading>
      {successMessageVisible && (
        <article className="message is-success">
          <div className="message-header">
            <p>{t("submitCapacity.success.headline")}</p>
            <button className="delete" aria-label="delete"></button>
          </div>
          <div className="message-body">
            <p>{t("submitCapacity.success.body")}</p>
            <p>{t("common.redirectToHome")}</p>
          </div>
        </article>
      )}

      <Card className="beds-input-hospital">
        <Card.Content>
          <Media>
            <Media.Content>
              <p className="title is-4">{name}</p>
              <p className="subtitle is-6">
                {address.postcode} {address.locality}
              </p>
            </Media.Content>
          </Media>
        </Card.Content>
      </Card>

      <div className="columns is-multiline">
        {resources.map(res => (
          <div className="column is-full is-one-third-fullhd" key={res.id}>
            <Card>
              <Card.Content>
                <BedsInput
                  key={res.id}
                  title={t(`resources.${res.type}`) || res.label}
                  resource={res}
                  updateResource={updateResource}
                />
              </Card.Content>
            </Card>
          </div>
        ))}
      </div>

      <Section>
        <Button.Group hasAddons={false} position={"right"} size={"medium"}>
          <Button renderAs="span" color="success" onClick={handleFormSubmit}>
            {t("common.save")}
          </Button>
          <Button renderAs="span" color="danger" onClick={handleCancel}>
            {t("common.cancel")}
          </Button>
        </Button.Group>
      </Section>
    </Container>
  ) : (
    <Redirect to="/" />
  );
};

export default EnterVacantBeds;
