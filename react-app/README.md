# Pilloww-Frontend

Frontend app.

## Dependencies

- nvm
- yarn

## How to start the App

```console
nvm use
yarn install
yarn start
```
